import { PieceInfo, PieceName, DamageType, Styling } from "@/models/pieceModels";

export const defaultPiece: PieceInfo = {
  name: PieceName.PIECE,
  currentHealth: 0,
  health: 0,
  damage: 0,
  damageType: DamageType.PHYSICAL,
  physicalDefence: 0,
  magicalDefence: 0,
  range: 0,
  movement: 0,
  movementTokens: 0,
  xPosition: 0,
  yPosition: 0,
  hasAttacked: false,
  isPlayerOne: false,
  styling: Styling.DEFAULT
};

export const defaultArcher: PieceInfo = {
  ...defaultPiece,
  name: PieceName.ARCHER,
  health: 60,
  currentHealth: 60,
  movement: 1,
  physicalDefence: 5,
  magicalDefence: 3,
  range: 3,
  damage: 35,
  movementTokens: 1,
  damageType: DamageType.PHYSICAL
};

export const defaultBishop: PieceInfo = {
  ...defaultPiece,
  name: PieceName.BISHOP,
  health: 50,
  currentHealth: 50,
  movement: 1,
  physicalDefence: 2,
  magicalDefence: 10,
  range: 3,
  damage: 30,
  movementTokens: 1,
  damageType: DamageType.MAGICAL,
  modifiedDamage: 15
};

export const defaultCavalry: PieceInfo = {
  ...defaultPiece,
  name: PieceName.CAVALRY,
  health: 60,
  currentHealth: 60,
  movement: 3,
  physicalDefence: 8,
  magicalDefence: 6,
  range: 1,
  damage: 25,
  movementTokens: 3,
  damageType: DamageType.PHYSICAL,
  modifiedDamage: 35
};

export const defaultSoldier: PieceInfo = {
  ...defaultPiece,
  name: PieceName.SOLDIER,
  health: 80,
  currentHealth: 80,
  movement: 2,
  physicalDefence: 10,
  magicalDefence: 4,
  range: 1,
  damage: 27,
  movementTokens: 2,
  damageType: DamageType.PHYSICAL
};

export const defaultWizard: PieceInfo = {
  ...defaultPiece,
  name: PieceName.WIZARD,
  health: 50,
  currentHealth: 50,
  movement: 1,
  physicalDefence: 3,
  magicalDefence: 6,
  range: 2,
  damage: 22,
  damageType: DamageType.MAGICAL,
  movementTokens: 1
};

export const getDefaultPieceInfo = (name: PieceName | null): PieceInfo => {
  switch (name) {
    case PieceName.ARCHER:
      return defaultArcher;
    case PieceName.BISHOP:
      return defaultBishop;
    case PieceName.CAVALRY:
      return defaultCavalry;
    case PieceName.SOLDIER:
      return defaultSoldier;
    case PieceName.WIZARD:
      return defaultWizard;
    default:
      return defaultPiece;
  }
};

export const listOfPieces: PieceInfo[] = [
  defaultArcher,
  defaultBishop,
  defaultCavalry,
  defaultSoldier,
  defaultWizard
];
