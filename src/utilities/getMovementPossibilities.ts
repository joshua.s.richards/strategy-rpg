import { PieceInfo, MovementPossibility, PieceName } from "@/models/pieceModels";
import { getDistanceBetweenPieces } from "./generalFunctions";

const getMovementPossibilities = (
  piecePositions: PieceInfo[][],
  pieceInfo: PieceInfo
): MovementPossibility[] => {
  const { xPosition: x, yPosition: y } = pieceInfo;
  const movementPossibilities: Set<string> = new Set();
  movementPossibilities.add(`${x}${y}`);

  for (let i = 0; i < pieceInfo.movementTokens; i++) {
    const size = movementPossibilities.size;
    for (let j = 0; j < size; j++) {
      const possibility = [...movementPossibilities.values()][j];
      addSingleRangeMovementPossibilities(
        movementPossibilities,
        piecePositions,
        Number(possibility.charAt(0)),
        Number(possibility.charAt(1))
      );
    }
  }

  return [...movementPossibilities.values()].map(possibility => {
    const xPosition = Number(possibility.charAt(0));
    const yPosition = Number(possibility.charAt(1));
    return {
      xPosition,
      yPosition,
      movementTokensUsed: getDistanceBetweenPieces(xPosition, x, yPosition, y)
    };
  });
};

const addSingleRangeMovementPossibilities = (
  movementPossibilities: Set<string>,
  piecePositions: PieceInfo[][],
  x: number,
  y: number
): void => {
  for (let i = x - 1; i <= x + 1; i++) {
    for (let j = y - 1; j <= y + 1; j++) {
      if (
        i >= 0 &&
        i < 8 &&
        j >= 0 &&
        j < 8 &&
        piecePositions[i][j].name === PieceName.PIECE &&
        ((i === x && j !== y) || (i !== x && j === y))
      ) {
        movementPossibilities.add(`${i}${j}`);
      }
    }
  }
};

export default getMovementPossibilities;
