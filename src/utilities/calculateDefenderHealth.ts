import { PieceInfo, DamageType, PieceName } from "@/models/pieceModels";

const calculateDefenderHealth = (attacker: PieceInfo, defender: PieceInfo) => {
  switch (attacker.name) {
    case PieceName.BISHOP:
      return bishopCalculateDefenderHealth(attacker, defender);
    case PieceName.CAVALRY:
      return cavalryCalculateDefenderHealth(attacker, defender);
    default:
      return defaultCalculateDefenderHealth(attacker, defender);
  }
};

const defaultCalculateDefenderHealth = (attacker: PieceInfo, defender: PieceInfo): number => {
  const defense =
    attacker.damageType === DamageType.PHYSICAL
      ? defender.physicalDefence
      : defender.magicalDefence;
  return defender.currentHealth - (attacker.damage - defense);
};

const bishopCalculateDefenderHealth = (attacker: PieceInfo, defender: PieceInfo): number => {
  const isNextToAlly =
    Math.abs(attacker.xPosition - defender.xPosition) +
      Math.abs(attacker.yPosition - defender.yPosition) ===
    1;
  const damage = isNextToAlly ? attacker.damage : attacker.modifiedDamage;
  return Math.min(defender.health, defender.currentHealth + (damage as number));
};

const cavalryCalculateDefenderHealth = (attacker: PieceInfo, defender: PieceInfo): number => {
  const damage = !attacker.movementTokens ? (attacker.modifiedDamage as number) : attacker.damage;
  return defender.currentHealth - (damage - defender.physicalDefence);
};

export default calculateDefenderHealth;
