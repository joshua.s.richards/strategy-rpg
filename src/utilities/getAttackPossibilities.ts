import { AttackPossibility, PieceInfo, PieceName } from "../models/pieceModels";
import { getDistanceBetweenPieces } from "./generalFunctions";

const getAttackPossibilities = (
  pieceInfo: PieceInfo,
  xPosition?: number,
  yPosition?: number
): AttackPossibility[] => {
  switch (pieceInfo.name) {
    case PieceName.ARCHER:
      return getArcherAttackPossibilities(
        pieceInfo,
        xPosition || pieceInfo.xPosition,
        yPosition || pieceInfo.yPosition
      );
    default:
      return getDefaultAttackPossibilities(
        pieceInfo,
        xPosition || pieceInfo.xPosition,
        yPosition || pieceInfo.yPosition
      );
  }
};

const getDefaultAttackPossibilities = (
  pieceInfo: PieceInfo,
  iconXPosition: number,
  iconYPosition: number
): AttackPossibility[] => {
  const attackPossibilities: AttackPossibility[] = [];
  for (let i = iconXPosition - pieceInfo.range; i < iconXPosition + pieceInfo.range + 1; i += 1) {
    for (let j = iconYPosition - pieceInfo.range; j < iconYPosition + pieceInfo.range + 1; j += 1) {
      if (
        getDistanceBetweenPieces(i, iconXPosition, j, iconYPosition) <= pieceInfo.range &&
        i > 0 &&
        i < 8 &&
        j > 0 &&
        j < 8
      ) {
        attackPossibilities.push({ xPosition: i, yPosition: j });
      }
    }
  }

  return attackPossibilities;
};

const getArcherAttackPossibilities = (
  pieceInfo: PieceInfo,
  iconXPosition: number,
  iconYPosition: number
): AttackPossibility[] => {
  const attackPossibilities: AttackPossibility[] = [];
  for (let i = iconXPosition - pieceInfo.range; i < iconXPosition + pieceInfo.range + 1; i += 1) {
    for (let j = iconYPosition - pieceInfo.range; j < iconYPosition + pieceInfo.range + 1; j += 1) {
      if (
        getDistanceBetweenPieces(i, iconXPosition, j, iconYPosition) === pieceInfo.range &&
        i > 0 &&
        i < 8 &&
        j > 0 &&
        j < 8
      ) {
        attackPossibilities.push({ xPosition: i, yPosition: j });
      }
    }
  }

  return attackPossibilities;
};

export default getAttackPossibilities;
