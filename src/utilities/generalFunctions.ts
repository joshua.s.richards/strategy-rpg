export const getDistanceBetweenPieces = (
  firstX: number,
  secondX: number,
  firstY: number,
  secondY: number
) => Math.abs(firstX - secondX) + Math.abs(firstY - secondY);
