export interface Position {
  xPosition: number;
  yPosition: number;
}

export interface AttackPossibility extends Position {}

export interface MovementPossibility extends Position {
  movementTokensUsed: number;
}

export enum Styling {
  DEFAULT,
  MOVEMENT_HIGHLIGHT,
  ATTACK_HIGHLIGHT
}

export interface PieceInfo {
  name: PieceName;
  health: number;
  currentHealth: number;
  movement: number;
  damage: number;
  modifiedDamage?: number;
  damageType: DamageType;
  physicalDefence: number;
  magicalDefence: number;
  range: number;
  movementTokens: number;
  hasAttacked: boolean;
  isPlayerOne: boolean;
  xPosition: number;
  yPosition: number;
  styling: Styling;
}

export enum DamageType {
  PHYSICAL,
  MAGICAL
}

export enum PieceName {
  ARCHER = "Archer",
  BISHOP = "Bishop",
  CAVALRY = "Cavalry",
  PIECE = "Piece",
  SOLDIER = "Soldier",
  WIZARD = "Wizard"
}
