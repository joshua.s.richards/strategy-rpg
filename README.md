# Strategy RPG

A turn-based strategy game written in Vue.js.

Rules:

*  The aim of the game is to defeat all of the opposing player's pieces.

*  Each player takes turns picking a unit from a list and placing it on their side of the board.

*  Then once each player has two pieces (plan is for 5 pieces per person but current setup is 2 for testing purposes),
each player takes their turns sequentially.

*  Each turn, the player is allowed to move each of their pieces once and attack once. However, you can't move after attacking.

*  When a unit attacks another, the defending unit takes damage equal to the attack damage of the attacking unit minus either
the physical or magical defence of the defending unit dependent on the attack type of the attacking unit.

*  When the health of a unit drops to 0 or below, the unit dies and is taken off the board.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
